# [rvv-qed-talk](https://gitlab.com/eidoom/rvv-qed-talk)

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7759592.svg)](https://doi.org/10.5281/zenodo.7759592)

[Live here](https://eidoom.gitlab.io/rvv-qed-talk/slides.pdf)

[Event](https://conference.ippp.dur.ac.uk/event/1104/) [contribution](https://conference.ippp.dur.ac.uk/event/1104/contributions/5674/)

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
