\documentclass[]{beamer}

\usetheme{Singapore}

\usepackage{caption}
% \usepackage{cleveref}
\usepackage{siunitx}
\usepackage{multirow}

\usepackage{tikz}
\usetikzlibrary{arrows.meta, backgrounds, fit}
\tikzset{
    eq/.style = {
        fill=blue!20,
        text width=12em,
        minimum height=2em,
        text centered,
        rounded corners,
        line width=0,
    },
    bound/.style = {
        fill=green!20,
        text width=13em,
        rounded corners,
        line width=0,
    },
    arrow/.style = {
        -{>[length=2mm, width=3mm]}
    },
    text label/.style = {
        text width=10em,
        align=center,
    }
}
\newcommand\light[1]{#1!20}

\newcommand{\pcite}[1]{\textcolor{gray}{\tiny\parencite{#1}}}

\usepackage[
    sorting=none,
    bibstyle=authoryear,
    citestyle=authoryearsquare,
    maxbibnames=10,
    maxcitenames=4,
    mincitenames=1,
    % uniquelist=false,
]{biblatex}
\addbibresource{bibliography.bib}
\renewcommand*{\bibfont}{\tiny}

\graphicspath{{./img/}}

\usefonttheme[onlymath]{serif}

\def\talktitle{Analytic two-loop amplitudes with finite fields}

\title{\talktitle}
\author{Ryan Moodie}
\institute{Turin University}
\date{N$^3$LO QED $\gamma^*\to \ell\bar \ell$ workstop: RVV \\\vspace{1em} 4 Aug 2022}
\titlegraphic{
    \begin{tikzpicture}[x=7em]
        \def\l{3em}
        \node at (0,0) {\includegraphics[width=\l,height=\l,keepaspectratio]{infn_logo}};
        \node at (1,0) {\includegraphics[width=\l,height=\l,keepaspectratio]{torino_logo}};
        \node at (2,0) {\includegraphics[width=\l,height=\l,keepaspectratio]{erc_logo}};
    \end{tikzpicture}
}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}{N$^3$LO QED $\gamma^*\to \ell\bar \ell$: RVV}
    \begin{center}
        \begin{tikzpicture}
            \node (image) {
                \includegraphics[width=0.9\textwidth]{n3lo-qed}
            };
            \node (text) at (2.5, -2) {
                \begin{minipage}{0.5\textwidth}
                    \begin{itemize}
                        \item $\gamma^*\to \ell\bar \ell\gamma$ at 2-loop?
                        \item Massive $\ell$?
                        \item Phenomenology?
                    \end{itemize}
                \end{minipage}
            };
        \end{tikzpicture}
    \end{center}
\end{frame}

\begin{frame}{Outline}{\talktitle}
    \tableofcontents
\end{frame}

\section[5-point 2-loop massless QCD amplitudes]{Five-point two-loop massless QCD amplitudes}

\begin{frame}{Outline}{\talktitle}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Five-point two-loop massless QCD amplitudes}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Major theoretical challenge
                    \begin{itemize}
                        \item Precision frontier
                        \item New methods
                    \end{itemize}
            \end{itemize}
            \begin{itemize}
                \item Reconstruct over finite fields
                    \begin{itemize}
                        \item[] \pcite{peraro:2019svx}
                    \end{itemize}
                \item Pentagon function basis
                    \begin{itemize}
                        \item[] \pcite{chicherin:2020oor}
                        \item[] \pcite{chicherin:2021dyp}
                    \end{itemize}
                \item Fast and stable implementations in \texttt{NJet}
                    \begin{itemize}
                        \item[] \pcite{njet}
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{jets_v1}
                \captionsetup{labelformat=empty}
                \caption{\pcite{collaboration:2114784}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Five-point two-loop massless QCD amplitudes}
    \begin{itemize}
        \item NNLO leading-colour $pp\to jjj$
            \begin{figure}
                \vspace{1ex}
                \includegraphics[width=7em]{jjj}
                \vspace{1ex}
            \end{figure}
        \item NLO full-colour $gg\to g\gamma\gamma$ (N$^3$LO $pp\to g\gamma\gamma$)
            \begin{columns}
                \begin{column}{0.5\textwidth}
                    \begin{figure}
                        \includegraphics[width=7em]{jjy}
                    \end{figure}
                \end{column}
                \begin{column}{0.5\textwidth}
                    \begin{figure}
                        \includegraphics[width=10em]{Mg1g2_2}
                    \end{figure}
                \end{column}
            \end{columns}
            \begin{itemize}
                \item[] \pcite{badger:2021imn}
                \item[] \pcite{badger:2021ohm}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Overview of our method}
    \begin{figure}
        \begin{center}
            \scalebox{0.8}{
                \begin{tikzpicture}[node distance=4em]
                    \def\ps{\{\boldsymbol{p}\}};
                    \def\f{\boldsymbol{f}\,(\ps)};
                    \def\mon{\textrm{mon}};
                    \def\adj{-5em};

                    \node (diagrams) [eq] {$\sum_i \mathrm{diagram}_i(\ps, \epsilon)$};
                    \node (integrands) [eq, right of=diagrams, xshift=14em] {$\sum_i \boldsymbol{a}_i(\ps, \epsilon) \cdot \boldsymbol{F}_i(\f)$};
                    \node (scalars) [eq, below of=integrands] {$\sum_i b_i(\ps, \epsilon) \,\mathcal{F}_i(\f)$};
                    \node (masters) [eq, below of=scalars] {$\sum_i c_i(\ps, \epsilon) \,\mathrm{MI}_i(\f)$};
                    \node (specials) [eq, below of=masters] {$\sum_i d_i(\ps, \epsilon) \,\mon_i(\f)$};
                    \node (remainders) [eq, below of=specials] {$\sum_i e_i(\ps) \,\mon_i(\f)$};
                    \node (rationals) [eq, below of=remainders] {$\sum_i r_i(\ps) \,\mon_i(\f)$};

                    \begin{scope}[on background layer]
                        \node [bound, fit=(integrands)(scalars)(masters)(specials)(remainders)(rationals)] (ff) {};
                    \end{scope}

                    \node (label) [rounded corners, minimum height=2em, minimum width=7em, fill=\light{green}, line width=0, left of=rationals, xshift=-8em] {Finite fields};
                    \path (label) edge [color=\light{green}, line width=0.5em] (rationals);

                    \path [arrow]
                    (diagrams.east) edge node [below] {Algebra} (integrands.west)
                    ([xshift=\adj] integrands.south) edge node [right] {Global integrand map} ([xshift=\adj] scalars.north)
                    ([xshift=\adj] scalars.south) edge node [right] {IBP identities} ([xshift=\adj] masters.north)
                    ([xshift=\adj] masters.south) edge node [right] {Special function basis} ([xshift=\adj] specials.north)
                    ([xshift=\adj] specials.south) edge node [right] {Subtract poles} ([xshift=\adj] remainders.north)
                    ([xshift=\adj] remainders.south) edge node [right] {Reconstruction} ([xshift=\adj] rationals.north)
                    ;

                    \node (hels) [below of=diagrams, yshift=4ex] {Helicity amplitudes};
                    \node (qgraf) [below of=hels, yshift=4ex, text label] {\texttt{qgraf}\\\pcite{nogueira:1991ex}};
                    \node (form) [below of=qgraf, yshift=2ex, text label] {\texttt{FORM}\\\pcite{ruijl:2017dtg}};
                    \node [left of=scalars, xshift=-12ex, text label] {\texttt{LiteRED}\\\pcite{lee:2012cn}};
                    \node [left of=label, xshift=-7ex, text label] {\texttt{FiniteFlow}\\\pcite{peraro:2019svx}};
                    \node [left of=masters, xshift=-12ex, text label] {Laporta\\\pcite{laporta:2000dsw}};
                    \node [left of=specials, xshift=-19ex, text label] {\texttt{PentagonFunctions++}\\\pcite{chicherin:2021dyp}};
                    \node (dipole) [left of=remainders, xshift=-16ex, text label] {Dipole scheme\\\pcite{catani:1996vz}};

                    \node [left of=dipole, xshift=-12ex, text label] {\texttt{Mathematica}};

                \end{tikzpicture}
            }
        \end{center}
    \end{figure}
\end{frame}

\section[Finite fields]{Finite field arithmetic}

\begin{frame}{Outline}{\talktitle}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Finite field arithmetic}
    \begin{itemize}
        \item Set of $n\in\mathbb{P}^p$ non-negative integers
            $$ \mathbb{F}_{n}=\{0,\ldots,n-1\} $$
        \item Arithmetic operations
            \begin{itemize}
                \item Addition modulo $n$
                \item Multiplication modulo $n$
                \item Inverses
            \end{itemize}
        \item Modular multiplicative inverse $x= a^{-1} \mod n$
            \begin{align*}
                a x &= 1 \mod n  & a&\neq0
            \end{align*}
        \item One-to-many map $\mathbb{Q}\to\mathbb{F}_{n}$
            $$ \frac{a}{b} \,\to\, a\, b^{-1} \mod n $$

    \end{itemize}
\end{frame}

\begin{frame}{Numeric representation for computation}
    \begin{itemize}
        \item \texttt{float}
            \begin{itemize}
                \item Catastrophic cancellation in large expressions
            \end{itemize}
        \item $\mathbb{F}_n$
            \begin{itemize}
                \item Fixed size integer
                \item No precision loss ($n$ large)
            \end{itemize}
        \item $\{\mathbb{F}_{n}\}\to\mathbb{Q}$
            \begin{itemize}
                \item Chinese Remainder Theorem
            \end{itemize}
        \item Amplitudes
            \begin{itemize}
                \item Large intermediate expressions
                \item Bypass with numerical evaluation over $\mathbb{F}_n$
                \item Reconstruct analytic expression
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Rational on-shell parametrisation}
    \begin{itemize}
        \item Momentum twistor variables
            $$\langle i j \rangle, [ij] \to x_i$$
            $$Z = \begin{pmatrix} \lambda_1 & \cdots & \lambda_n \\ \mu_1(\tilde\lambda_\text{adj}) & \cdots & \mu_n(\tilde\lambda_\text{adj}) \end{pmatrix}$$
            \item Rational functions
                \begin{itemize}
                    \item $p_i(\boldsymbol{x}), s_{ij}(\boldsymbol{x}), \ldots$
                \end{itemize}
            \item $\boldsymbol{x}$ unconstrained
                \begin{itemize}
                    \item On-shell
                    \item Momentum conserving
                \end{itemize}
    \end{itemize}
\end{frame}

\section[Reconstruction]{Reconstruction methods}

\begin{frame}{Outline}{\talktitle}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Reconstruction}
    \begin{itemize}
        \item Amplitude components
            $$
            F(\boldsymbol{x}) = \sum_{i} r_i(\boldsymbol{x}) \, \text{mon}_i(f)
            $$
        \item Have numerical algorithm for $r_i$
        \item \texttt{FiniteFlow} \pcite{peraro:2019svx}
            \begin{figure}
                \vspace{1ex}
                \includegraphics[width=0.5\textwidth]{black-box}
                \vspace{1ex}
            \end{figure}
        \item Reconstruct expression from sufficient evaluations
        \item Strategies to optimise and compactify
    \end{itemize}
\end{frame}

\begin{frame}{Linear relations in the coefficients}
    \begin{itemize}
        \item Linearise the $r_i$
            $$
            \{ r_i \}_{i\in S} \to \{ r_i \}_{i\in T \subseteq S}
            $$
        \item Numerically solve:
            $$
            \sum_i a_i \, r_i(\boldsymbol{x})  = 0
            $$
            and choose lowest degrees
        \item Ans\"atze, eg permutations of $5g$ $r_i$ for $3g2\gamma$:
            $$
            \sum_i a_i \, r_i(\boldsymbol{x}) + \sum_j b_j \, e_j(\boldsymbol{x}) = 0
            $$
    \end{itemize}
\end{frame}

\begin{frame}{Matching factors}
    \begin{itemize}
        \item Coefficient ansatz
            $$
            r(\boldsymbol{x}) = \frac{n(\boldsymbol{x})}{\prod_{k} {\ell_k}^{e_k}(\boldsymbol{x})}
            \qquad
            \ell_k\in\text{pentagon alphabet}
            $$
        \item Determine $e_k$ by reconstructing $r$ on univariate slice
            $$
            \boldsymbol{x} = \boldsymbol{c}_0 + \boldsymbol{c}_1 t \qquad \rightsquigarrow \qquad r(t)
            $$
            and matching RHS
        \item Fix denominators
            $$
            \{ r_i \} \to \{ n_i \}
            $$
    \end{itemize}
\end{frame}

\begin{frame}{Univariate partial fraction decomposition}
    \begin{itemize}
        \item Example in $y$
            $$
            \frac{N(x,y)}{x^2y^2(x^2+y^2)} =\frac{q_1(x)}{y}+\frac{q_2(x)+q_3(x)y}{y^2}+\frac{q_4(x)+q_5(x)y}{x^2+y^2}
            $$
            \vspace{-2ex}
            \begin{itemize}
                \item Needs no knowledge of analytic form of $N$
                \item Choose $y$ by studying one-loop
            \end{itemize}
        \item Reconstruct $r_i$ directly in decomposed form
            $$
            \{ r_i(\overline{\boldsymbol{x}},y) \} \to \{ q_i(\overline{\boldsymbol{x}}) \}
            $$
            \vspace{-3ex}
            \begin{itemize}
                \item Reduce variables by one
                \item Lower degrees
            \end{itemize}
        \item Simplifies reconstruction
            \begin{itemize}
                \item $\times 10$ point numerical evaluation time (linear fit in $\{q_i\}$ over $y$)
                \item $\times \frac{1}{100}$ samples required
            \end{itemize}
    \end{itemize}
\end{frame}

\section[Performance]{Implementation and performance}

\begin{frame}{Outline}{\talktitle}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Implementation}
    \begin{itemize}
        \item Finite remainders coded up in \texttt{C++} as analytic expressions
        \item Construct partial amplitudes as
            \begin{equation*}
                F^{h} = r^h_{i}(\boldsymbol{x}) \, M^h_{ij} \,\, f^h_j
            \end{equation*}
            \begin{tabular}{lll}
                $f^h_j$ & special function monomials & global \\
                $M^h_{ij}$ & rational sparse matrices & partials \\
                $r^h_{i}$ & independent rational coefficients & helicities \\
                $\boldsymbol{x}$ & momentum twistor variables & global \\
            \end{tabular}
            \vspace{1ex}
        \item Independent helicities permuted to all mostly-plus
            \begin{itemize}
                \item Mostly-minus: ${r_{i}}^* \, M_{ij} \,\, P(f_j)$
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Evaluation strategy}
    \begin{figure}
        \includegraphics[width=\textwidth]{eval_hor}
    \end{figure}
\end{frame}

\begin{frame}{Timing}
    \begin{table}
        \begin{tabular}{| l | r r | r r |}
            \hline
            \multicolumn{1}{|c|}{\multirow{2}*{Channel}} & \multicolumn{2}{c|}{\texttt{f64/f64}} & \multicolumn{2}{c|}{Evaluation strategy} \\
            \cline{2-5}
            & Time (s) & s.f.~(\%) & Time (s) & s.f.~(\%) \\
            \hline
            $gg \to ggg$                         & 1.39 & 69 & 1.89 & 77 \\
            $gg \to \bar{q}qg$                   & 1.35 & 91 & 1.37 & 91 \\
            $qg \to qgg$                         & 1.34 & 92 & 1.57 & 93 \\
            $q\bar{q} \to ggg$                   & 1.34 & 93 & 1.38 & 93 \\
            $\bar{q}Q \to Q\bar{q}g$             & 1.14 & 99 & 1.16 & 99 \\
            $\bar{q}\bar{Q} \to \bar{q}\bar{Q}g$ & 1.36 & 99 & 1.39 & 99 \\
            $\bar{q}g \to \bar{q}Q\bar{Q}$       & 1.36 & 99 & 1.39 & 99 \\
            $\bar{q}q \to Q\bar{Q}g$             & 1.14 & 99 & 1.14 & 99 \\
            $\bar{q}g \to \bar{q}q\bar{q}$       & 1.84 & 99 & 1.90 & 99 \\
            $\bar{q}\bar{q} \to \bar{q}\bar{q}g$ & 1.82 & 99 & 1.94 & 99 \\
            $\bar{q}q \to q\bar{q}g$             & 1.71 & 99 & 1.77 & 99 \\
            \hline
            $gg\to\gamma\gamma g$ $*$   & 9    & 99 & 26   & 99 \\
            \hline
        \end{tabular}
    \end{table}
\end{frame}

\begin{frame}{Stability}
    \begin{columns}
        \begin{column}{1.125\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{stability_all}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{IR performance}
    \begin{figure}
        \includegraphics[width=\textwidth]{collinear}
    \end{figure}
\end{frame}

\section[Masses]{Masses}

\begin{frame}{Outline}{\talktitle}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Single external mass at 2-loop (planar)}
    \begin{itemize}
        \item Applications of our pipeline

            \vspace{1ex}
            \begin{tabular}{lll}
                $p p \to H b \bar b$ & & \pcite{badger:2021ega} \\
                $p p \to W b \bar b$ & (on-shell $W$) & \pcite{badger:2021nhg} \\
                $p p \to W \gamma j$ & & \pcite{badger:2022ncb} \\
                $p p \to W(\ell\nu) b \bar b$ & $d\sigma$ & \pcite{hartanto:2022qhh} \\
            \end{tabular}
            \vspace{1ex}

        \item Others

            \vspace{1ex}
            \begin{tabular}{lll}
                $p p \to (W/Z/\gamma^*) j j$ & & \pcite{abreu:2021asb} \\
            \end{tabular}
            \vspace{1ex}

        \item Integrals

            \begin{itemize}
                \item \texttt{PentagonFunctions++} \pcite{chicherin:2021dyp}
                \item \texttt{DiffExp} \pcite{hidding:2020ytt}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Internal masses}
    \begin{itemize}

        \item Applications of our pipeline (planar)

            \vspace{1ex}
            \begin{tabular}{lll}
                $g g \to t \bar t$ & 2-loop & \pcite{badger:2021owl} \\
                $p p \to t \bar t j$ & 1-loop $\mathcal{O}(\epsilon^2)$& \pcite{badger:2022mrb} \\
            \end{tabular}
            \vspace{1ex}

        \item Others

            \vspace{1ex}
            \begin{tabular}{lll}
                $gg\to Hj$ & 2-loop $d\sigma$ & \pcite{bonciani:2022jmb} \\
                % $H\to ZZ$ & integrals &\pcite{chaubey:2022hlr} \\
                % $H\to WW$ &  &\pcite{ma:2021cxg} \\
                $q\bar q \to Q\bar Q$ & 2-loop & \pcite{mandal:2022vju} \\
            \end{tabular}

        \item Integrals

            \begin{itemize}
                \item Differential equation \pcite{henn:2013pwa}, iterated integrals \pcite{chen:1977oja}
                \item \texttt{AMFlow} \pcite{liu:2022chg}
            \end{itemize}

    \end{itemize}
\end{frame}


\section{Conclusion}

\begin{frame}{Outline}{\talktitle}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Conclusion}
    \begin{itemize}
        \item Two-loop amplitude computation with finite fields
            \begin{itemize}
                \item Established for massless to $2\to3$
                \item First massive results appearing
            \end{itemize}
        \item Promising outlook for RVV $\gamma^*\to \ell\bar \ell$
        \item Integrals

            \begin{tabular}{ll}
                $m_l=0$ & Literature \pcite{gehrmann:2000zt,gehrmann:2001ck} \\
                $m_l\neq0$ & \texttt{pySecDec} \pcite{borowka:2017idc}\\
                & \texttt{DiffExp} \pcite{hidding:2020ytt}\\
            \end{tabular}

    \end{itemize}
    \hfill
    \begin{figure}
        \includegraphics[width=0.8\textwidth]{rvv}
    \end{figure}
\end{frame}

\begin{frame}[allowframebreaks]{Bibliography}
    \printbibliography
\end{frame}

    \end{document}
